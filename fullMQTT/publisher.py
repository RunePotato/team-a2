import paho.mqtt.client as mqtt
import random
import time
import json

mqttBroker = "teama2.eastus.cloudapp.azure.com"
client = mqtt.Client("Client 1")
client.connect(mqttBroker)
print("Connected to the MQTT broker.")
ID = 0

while True:
    try:
        randomNumber = random.randint(0,101)
        data = {
            "Name" : "Random Number",
            "ID" : ID,
            "Temperature" : randomNumber

        }
        client.publish("Temperature", json.dumps(data))
        print("Random number", str(randomNumber), "published.")
        ID += 1
        time.sleep(2)
    except(KeyboardInterrupt):
        client.disconnect(mqttBroker)
        print("Connection disconnected. ")
        break
    