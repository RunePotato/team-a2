<h1> MICROSOFT AZURE MQTT BROKER </h1>

Setup was done with steps from the [Project Excercises for week 08 gitlab page](https://eal-itt.gitlab.io/21s-itt2-project/exercises/exercises_ww08).

The MQTT broker works only if the creator of the Azure Virtual Machine (Julius) turns it on.

The code in this directory also contains the same code as in the directory above.

The MQTT new MQTT broker is : teama2.eastus.cloudapp.azure.com

**NOTE**

In order to set up the VM, default setting were used with Debian 10 kernel with backports was used.

**_IMPORTANTER NOTE_**

Apparently the VM costs money, so remind the creator to turn it off after it is done using.
