import paho.mqtt.client as mqtt
import random
import time

print("Connecting to the MQTT broker...")
mqttBroker = "teama2.eastus.cloudapp.azure.com"
client = mqtt.Client("Client 1")
client.connect(mqttBroker)
print("Connected to the MQTT broker.")

while True:
    try:
        randomNumber = random.randint(0,101)
        client.publish("Random Number", randomNumber)
        print("Random number", str(randomNumber), "published.")
        time.sleep(2)
    except(KeyboardInterrupt):
        client.disconnect(mqttBroker)
        print("Connection disconnected. ")
        closing = input("Press Enter to close: \n")
        break
    
