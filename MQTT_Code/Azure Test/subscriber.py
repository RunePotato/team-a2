import paho.mqtt.client as mqtt
import time

def on_message (client, userdata, message):
    print("Recieved:", str(message.payload.decode("utf-8")))

mqttBroker = "teama2.eastus.cloudapp.azure.com"
client = mqtt.Client("Client 2")
print("Connecting to MQTT broker...")
client.connect(mqttBroker)
print("Connected to the MQTT broker.")

client.loop_start()
client.subscribe("Random Number")
client.on_message = on_message
client.loop_stop()
