**Scrum master:**

Scrum master is the center of the scrum, coordinating all project activities and linking customers and different teams that are part of the project, also called “heart of the scrum process”. Scrum master is like a lighthouse in scrum team which is composed of product owner, development team and the scrum master himself or herself. The scrum master has several roles and responsibilities in the project. He/she collaborates with all the members of the team, and for each specific role.

**Responsibilities of Scrum master, Facilitator and Secretary:**

Scrum master removes or clears top tickles and impediments in scrum terms. An impediment is anything that keeps the development theme for making decision or moving forward with the task that we are doing. Obstacles can come in many forms and from different direction like for instance, it could be technical or operational issues, lack of knowledge or it can also be missing project resources. Basically, if the scrum team is stuck on an issue and requires outside opinion for help, the scrum master needs to help produce that assistance right away. This way, he/she removes impediments and obstacles that might hinder the progress and the speed of the team. Scrum master also responsibilities to take several forms. For example, he/she ensures that each person on the team receives an equal voice, they have their choice or chance to voice their opinion.

Scrum master is team’s facilitator. Facilitator itself tells everything, a scrum master facilitates and organizes all scrum events requested by the product owner and development team as when needed. he/she is responsible for cultivating the light walking environment for the entire team. This includes everything from a physical workspace to communication tools, to make sure that the team follows scrum rules. he/she facilitates the team in making important decision that would increase the productivity of team where everyone feels safe sharing their viewpoint and rest of the members in team value that viewpoint.

Secretary plays a supportive role in the scrum project where he/she performs a wide variety of tasks, prepare documents, memo and create an agenda to inform all the team members about oncoming meetings.
Team role rotation plan:

