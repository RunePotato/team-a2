# **Datacenter power solutions:**

* Power monitoring system

* Emergency back-up system 

* Emergency detection system 

* Thermo sensors for power boxes or cables 

* Power saving solutions 

* Humidity sensors in “power rooms”

* Fuse box like in home 

* Server Virtualization 

* Battery measurement system

* Battery testing system

* Energy efficiency solutions (monitor what servers are being used 
  to save power by moving traffic to fill up servers, and shut down those not being used)

* Redundancies 


# **Requirements:**

* Thermo sensor to monitor heat in cable room etc.
* Python running with venv to test stuff
