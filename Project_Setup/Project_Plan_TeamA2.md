---
title: '21S ITT2 Project'
subtitle: 'Project plan, part I'
authors: [Julius, Oscar, Benjamin, Balazs, Manisha, Greta, Rune]
date: 3.2.2021
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

# Background

This is the semester ITT2 project where we will work with different projects, initated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.

The overall project case is described at [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

# Purpose

The main goal is to have a system where IoT sensors collect data from a datacenter.
This is a leaning project and the primary objective is for us to get a good understanding of the challenges involved in making such a system and to give us hands-on experience with the implementation.

For simplicity, the goals stated will evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

# Goals

The overall system that is going to be build looks as follows:  
![project_overview](../docs/datacenter_iot_system_overview.png "ITT2 project overview")  
Reading from the left to the right:  

* Sensor modules 1-3: A placeholder for x number of sensors
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.  
* Computer: Connects to both the Raspberry Pi and Gitlab while developing and troubleshooting  
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things. 
* Consumer: 


Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* Docmentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation 
* TBD: Fill in more, as agreed, with teams from the other educations


# Schedule

See the [lecture plan](https://eal-itt.gitlab.io/21s-itt2-project/other-docs/21S_ITT2_PROJECT_lecture_plan.html) for details.

# Organization

[Considerations about the organization of the project may include:  
     Identification of the steering committee  
     Identification of project manager(s)  
     Composition of the project group(s)  
     Identification of external resource persons or groups  

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear.  This could be done in relation to the goals of the project.]

So far, there are no roles filled for organizational leader and project managers, which we will adapt to when we start working with the other educational institution and the engineers from a company with which we have to cooperate with.


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

[The risks involved in the project is evaluated, and the major issues will be listed.  
The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.]

The risks listed in the pre-mortem in the project gitlab are:

 
* Unrealistic expectations/unrealistic goals.  setting unrealistic goals for the project within a way  to short timeframe, may cause a pressure to skip ahead or rush through the planned. phases to meet the schedule in time. that can and will affect the product.
* We will need to set a bare minimum, that will be realistic to achieve.

* If the minimum (requirements is met) we should have extra things Scheduled that we can work further on, so we can use our time best possible when present in group.

* Miscommunication meaning deadlines are not met creating an unfinished project.

* Members of the group dont contribute enough, and the workload becomes to large for the member contributing to be able to finish on their own.
* Last semester we had some dropouts without warning making it hard to know if they would be there or contribute (this might happen again?) this could also be general absence because of personal issues or illness etc.

* Corona Restrictions (this point is slightly outdated as we should by now know how to get some things done even in a lockdown)

* Simple tasks suddenly become much larger then first believed creating issue and making deadlines much harder to meet.

* With the addition of work across educations we could end in a situation where the other groups do not do their part of the workload and therefore, we are stuck and cant finish etc.

* Wrong work distribution - if the task assigned for person who does not have all necessary knowledges or at least basic knowledges to do the task.


# Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders  
     Internal vs. external  
     Positive vs. negative  
     Active vs. passive  

A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.  
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

In this instance, it is the company and the students from the other education and also teachers from our education. We could handle their most likely different interests by negotiating with them and striking a compromise in which we can fully engage to.

# Communication

[The stakeholders may have some requirements or requests as to what reports or other output is published or used. Some part of the project may be confidential.  
Whenever there is a stakeholder, there is a need for communication. It will depend on the stakeholder and their role in the project, how and how often communication is needed.  
In this section, reports, periodic emails, meetings, facebook groups and any other stakeholder communication will be described]  

We will use Discord, Zoom and other platforms for communication, if necessary with the different groups assigned to this project.

# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]  

We will evaluate the project by how successful and complete it is. What also comes into the factor of success is how useful it can be.

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion


* Common project group on gitlab.com

  The project is managed using gitlab.com. The groups is at [https://gitlab.com/21s-itt2-datacenter-students-group](https://gitlab.com/21s-itt2-datacenter-students-group)
