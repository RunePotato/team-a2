Pre-Mortem

-	Unrealistic expectations/unrealistic goals.  setting unrealistic goals for the project within a way  to short timeframe, may cause a pressure to skip ahead or rush through the planned. phases to meet the schedule in time. that can and will affect the product.
We will need to set a bare minimum, that will be realistic to achieve.
If the minimum (requirements is met) we should have extra things Scheduled that we can work further on, so we can use our time best possible when present in group. 

-	Miscommunication meaning deadlines are not met creating an unfinished project.

-	Members of the group don’t contribute enough, and the workload becomes to large for the member contributing to be able to finish on their own.

-	Last semester we had some dropouts without warning making it hard to know if they would be there or contribute (this might happen again?) this could also be general absence because of personal issues or illness etc. 

-	Corona Restrictions (this point is slightly outdated as we should by now know how to get some things done even in a lockdown)

-	Simple tasks suddenly become much larger then first believed creating issue and making deadlines much harder to meet.

-	With the addition of work across educations we could end in a situation where the other groups do not do their part of the workload and therefore, we are stuck and can’t finish etc. 

-	Wrong work distribution - if the task assigned for person who does not have all necessary knowledges or at least basic knowledges to do the task.





