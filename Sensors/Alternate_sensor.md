https://www.seeedstudio.com/Grove-Thermal-Imaging-Camera-IR-Array-MLX90641-110-degree-p-4612.html

This MLX90641 infrared camera would have been a very nice addition to the project but we were unable to find any in stock on the websites we had available and therefore could not order this specific sensor. For a future project the addition of this sensor or one like it could greatly improve the end goal
