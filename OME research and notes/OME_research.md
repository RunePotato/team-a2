<h1>OME RESEARCH</h1>

The OMEs (Operational Maintenance Engineers) are our colleagues on the Datacenter project. They are from the Fredericia Maskinmesterskole (whatever that means, danish speaking guys can write translation here in the parantheses).


<h2>Learning Goals:</h2>

- Interdisciplinary elements, including methodology
- Thermal machines and systems
- Electrical and electronic machinery, plant and equipment
- Process analysis and automation
- Management, finance and security
- Professional internship for 3 months
- Bachelor project
- Electives (stated below)

<h2>Educational level:</h2>

- Since the admission requirements are stated to accomodate for people who ended high schools (gymnasiums), then it should be assumed that the OME education is a bachelor degree level.

<h2>Courses:</h2>

- Maritime line : ship engine maintenance

- Energy at sea : offshore power (oil, gas, windmills, watermills, etc.)

- Industry and management : speaks for itself

- Data Center Program: management of a datacenter operation

<h2>Job Occupations:</h2>

After their study, they can obtain one of the following certificates:

- Authorized electrician
- Boiler fitter certificate see here for application for certificate
- Cooling authorization
- Authorization for adjustment of larger gas boiler
- Certificate as an expert in accordance with the Executive Order on Pressure Vessels
- Seafarers' certificates as an engineer on duty
- Ship engineer of 1st/2nd degree
- Ship chief

<h2>Potential Challenges:</h2>

- Only able to communicate at certain times during the week

- Not be able to properly answer questions in English (since they are on a Danish course)

<h2>Other Notes:</h2>

**OPEN FOR EDITING**
