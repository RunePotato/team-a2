<h1>**_Minimum viable product TEAM A2_**</h1>

<h2>Product name</h2> 

**???**

(HMWS (Heat Monitoring and Warning System) suggested.)


<h2>Features</h2>

- Temperature measuring via DHT 11 sensor
- Temperature measuring via a thermal cameraa (experimental)
- Constant recording of the values via Python program
- Sending the values via MQTT protocol to an offshore Database (MongoDB) and visualizing them.
- Send warnings if the system reaches critical temperatures
- Ability to turn off/optimize the temperature of the system (experimental)

<h2>Early customers:</h2>

- Operational Maintenance Engineers (OMEs)

<h2>Use Case / Testing:</h2>


- System is designed to be used in a Datacenter power system to monitor either a certain point's temperature in the power system, or the entire system itself.
- Testing unknown as of now, will be decided later (whether in an actual datacenter, or just as a test at home/school)

