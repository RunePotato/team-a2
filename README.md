# Team A2

Team A2 ITT2 datacenter project

Members
- Benjamin Bom Christensen (@Benjaminbom)
- Manisha Gurung (@GurungManisha)
- Greta Sumskaite (@Gretha)
- Julius Ingeli (@SolidSnakeSVK)
- Rune Petersen (@RunePotato)
- Balázs Erdős (@Bule90)
- Oscar Harttung (@Oscar.Hartt)

**Link to Pre-Mortem:**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/blob/master/Project_Setup/Pre-Mortem_Team_A2.md

**ITT2 Powercase - Brainstorming 2nd Project 10-02-2021:**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/blob/master/Project_Setup/ITT2%20Powercase%20-%20Brainstorming%202nd%20Project%2010-02-2021.md

**ITT2 Powercase - UseCaseDiagram 2nd Project 10-02-2021**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/blob/master/Project_Setup/UseCaseDiagram.jpg


**OME research notes:**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/blob/master/OME%20research%20and%20notes/OME_research.md

**MQTT research notes**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/blob/master/Project_Setup/MQTT_research.md


**MQTT test programs**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/tree/master/MQTT_Code


**Microsoft Azure setup**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/tree/master/MQTT_Code/Azure%20Test

**Scrum meeting tasks and responsibilites**

https://gitlab.com/21s-itt2-datacenter-students-group/team-a2/-/blob/master/Project_Setup/Scrum_meetings_tasks_and_responsibilities.md

**Proof of Concept video**

https://www.youtube.com/watch?v=BnPL5qaK8PE

